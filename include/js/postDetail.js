var id = getUrlVars()["id"];
var xmlhttp = new XMLHttpRequest();
var url = "../content/data/wp_posts.json";
var productObject;

xmlhttp.open("GET", url, true);
xmlhttp.send();

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        //This should be like a hashmap.
        var productObjectArray = JSON.parse(this.responseText);
        for(var i in productObjectArray) {
          if(productObjectArray[i].ID == id) {
            productObject = productObjectArray[i];
          }
        }
        populateData();
        loadImage();
    }
};


function populateData() {
  if(productObject == null) {
    return;
  }
   document.getElementById("post_title").innerHTML = productObject.post_title;
   document.getElementById("post_published_date").innerHTML = productObject.post_date;

   document.getElementById("post_content").innerHTML = productObject.post_content;

   document.getElementById("image_product_detail_expand").href=productObject.image_main;

   //document.getElementById("pellet_name_category").innerHTML = productObject.name_es;
   //document.getElementById("name_product_detail").innerHTML = productObject.name_es;
   //document.getElementById("caliber_product_detail").innerHTML = productObject.caliber;
   //document.getElementById("weight_product_detail").innerHTML = productObject.weight;
   //document.getElementById("especification_product_detail").innerHTML = productObject.especification_es;
   //document.getElementById("presentation_product_detail").innerHTML = productObject.presentation_es;
   //document.getElementById("categories_product_detail").innerHTML = productObject.category + ", " + productObject.caliber;
   //document.getElementById("presentation_product_detail").innerHTML = productObject.presentation_es;
   //document.getElementById("product_detail_id").innerHTML = productObject.product_id;

   loadImageGallery(productObject.post_gallery, productObject.post_gallery_expand);
}

function loadImage() {
  if(productObject == null) {
    return;
  }
  var mainImage = new Image;
  mainImage.src = productObject.image_main;
  var image_product_detail = document.getElementById('image_product_detail');
  mainImage.onload = function() {
    image_product_detail.src = this.src;
  }
}

function loadImageGallery(galleryImages, galleryImagesExpand) {
  if(galleryImages == null) {
    return;
  }
  var imageGallery = galleryImages.split(',');
  var imageGalleryExpand = galleryImagesExpand.split(',');
  var html_image_gallery_div = document.getElementById('image_gallery_div');
  var post_content = "";

  for(var i in imageGallery) {
    post_content += '<dl class="gallery-item"><dt class="gallery-icon landscape"><a href="../content/image/blog/'+ imageGalleryExpand[i] +'">';
    post_content += '<img src="../content/image/blog/'+ imageGallery[i] +'" alt="" /></a></dt><dd></dd></dl>';
  }
  html_image_gallery_div.innerHTML = post_content;
}
