var xmlhttp = new XMLHttpRequest();
var url = "../content/data/products.json";
var lang = getUrlVars()["lang"]; 
if (lang == null) {
  lang = "es";
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var productArray = JSON.parse(this.responseText);
        populateProducts(productArray);
    }
};

xmlhttp.open("GET", url, true);
xmlhttp.send();

function populateProducts(productArray) {
  var productItem = "";
  var productName = "";
  var i;

    for (i = 0; i < productArray.length; i++) {
        if (productArray[i].publish_main == "true") {

        productName = productArray[i].name[lang] + "<br/>(" + productArray[i].caliber + ")";

        productItem += '<!-- Product Item --><li class="product column-1_3 "><div class="post_item_wrap"><div class="post_featured"><div class="post_thumb">';
        productItem += '<a href="../es/product-detail.php?id='+productArray[i].id+'">';
        if (productArray[i].new == "true") {
          productItem += '<span class="onsale">NUEVO!</span>';
        }
        productItem += '<img src="' + productArray[i].image_small + '" alt="" title="Product" />';
        productItem += '</a></div></div><div class="post_content"><h2 class="woocommerce-loop-product__title"><a id="product_name_id1" href="../es/product-detail.php?id='+productArray[i].id+'">'+productName+'</a></h2>';
        productItem += '<a href="../es/product-detail.php?id='+productArray[i].id+'" class="button add_to_cart_button">VER MÁS</a></div></div></li><!-- /Product Item -->';
      }
    }
    document.getElementById("product_list_id").innerHTML = productItem;
}

function openProductDetail(productId) {
  window.open("../es/product-detail.php?id='" + productId + "'","mywindow");
}

//Btn pestaña chile

//Get the button
var mybutton = document.getElementById("chileButton");
mybutton.style.display = "block";

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop >= 300 || document.documentElement.scrollTop >= 300) {
       //mybutton.style.display = "block";
  } else {
    //mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}


//Blog section
var xmlhttp = new XMLHttpRequest();
var url = "../content/data/wp_posts.json";

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var productArray = JSON.parse(this.responseText);
        populateMainPost(productArray);
        populatePosts(productArray);
    }
};

xmlhttp.open("GET", url, true);
xmlhttp.send();

function populateMainPost(productArray) {
  loadImage(productArray[0].image_small);
  document.getElementById("mainPostImageLink").href="post.php?id=" + productArray[0].ID;
  document.getElementById("mainPostLink").href="post.php?id=" + productArray[0].ID;
  document.getElementById("mainPostTitle").href="post.php?id=" + productArray[0].ID;
  document.getElementById("mainPostTitle").innerHTML = productArray[0].post_title;
  document.getElementById("mainPostDate").innerHTML = productArray[0].post_date;
  document.getElementById("mainPostDescription").innerHTML = productArray[0].post_content_home + " ...";
}

function loadImage(imageURL) {
  if (imageURL == null) {
    return;
  }
  var mainImage = new Image;
  mainImage.src = imageURL;
  var image_product_detail = document.getElementById('mainPostImage');
  mainImage.onload = function() {
    image_product_detail.src = this.src;
  }
}


function populatePosts(productArray) {
  var postItem = "";
  var i;

  for (i = 1; i <= 4; i++) {
    postItem += '<article class="post_item post_accented_off"><div class="post_featured"><div class="post_thumb"><a class="hover_icon hover_icon_link" href="post.php?id='+ productArray[i].ID +'">';
    postItem += '<img alt="Rental Firearms &#038; Fees" src="'+ productArray[i].image_small +'"></a></div></div><div class="post_header">';
    postItem += '<h6 class="post_title"><a href="post.php?id='+ productArray[i].ID +'">' + productArray[i].post_title + '</a></h6><div class="post_meta"><span class="post_meta_date">Publicado <a href="#">'+ productArray[i].post_date +'</a>';
    postItem += '</span></div></div></article>';
  }
    document.getElementById("mainPostItems").innerHTML = postItem;
  }

