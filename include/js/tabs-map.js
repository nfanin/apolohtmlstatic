var map;
var iconBase = '../content/image/marker.png';
var localResults;

function initMap() {
    
    //******************* ARGENTINA MAP ****************************
    map = new google.maps.Map(document.getElementById('argentinaMap'), {
		zoom: 5,
		center: new google.maps.LatLng(-35.7033211,-61.569857),
		mapTypeId: 'roadmap'
	});

	var script = document.createElement('script');
	script.src = '../content/data/clients.json';
	document.getElementsByTagName('head')[0].appendChild(script);
	
	window.eqfeed_callback = function(results) {
		var infowindow = new google.maps.InfoWindow();
		console.log(results.features.length);
		for (var i = 0; i < results.features.length; i++) {
			var id = results.features[i].id;
			var title = results.features[i].properties.title;
			var content = "<div class=''><b>" + title + "</b></div><br/>" + results.features[i].properties.description;
			var coords = results.features[i].geometry.coordinates;
			var latLng = new google.maps.LatLng(coords[0],coords[1]);

			var marker = new google.maps.Marker({
				position: latLng,
				map: map,
				icon: iconBase,
				title: title,
				id: id
			});
			google.maps.event.addListener(marker,'click', (function(marker,content,infowindow) {
			        return function() {
			           infowindow.setContent(content);
			           infowindow.open(map, marker);
			        };
			})(marker,content,infowindow));
		}
	}
      
    var americaMap = new google.maps.Map(document.getElementById('americaMap'), {
        zoom: 2,
        center: new google.maps.LatLng(15.6326819,-88.9715594),
        mapTypeId: 'roadmap',
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        scaleControl: false,
        zoomControl: false
    });


    var africaMap = new google.maps.Map(document.getElementById('africaMap'), {
        zoom: 2.5,
        center: new google.maps.LatLng(2.4255243,6.2055335),
        mapTypeId: 'roadmap',
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        scaleControl: false,
        zoomControl: false
    });

    var asiaMap = new google.maps.Map(document.getElementById('asiaMap'), {
        zoom: 2.5,
        center: new google.maps.LatLng(29.1611917,72.8361715),
        mapTypeId: 'roadmap',
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        scaleControl: false,
        zoomControl: false
    });

    var eurpeMap = new google.maps.Map(document.getElementById('europeMap'), {
        zoom: 4,
        center: new google.maps.LatLng(46.6567038,4.1908022),
        mapTypeId: 'roadmap',
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        scaleControl: false,
        zoomControl: false
    });

    var oceaniaMap = new google.maps.Map(document.getElementById('oceaniaMap'), {
        zoom: 3,
        center: new google.maps.LatLng(-29.7654026,145.0407565),
        mapTypeId: 'roadmap',
        streetViewControl: false,
        mapTypeControl: false,
        fullscreenControl: false,
        scaleControl: false,
        zoomControl: false
    });

    /**var americaScript = document.createElement('script');
	americaScript.src = '../content/data/america_points.json';
    document.getElementsByTagName('head')[0].appendChild(americaScript);
    window.points = function(results) {
        var infowindow = new google.maps.InfoWindow();
        var americaMapPoints;
        var africaMapPints;
        var europeMapPoints;

        for (var i = 0; i < results.features.length; i++) {
            var id = results.features[i].id;
			var title = results.features[i].title;
			var content = "<div class=''><b>" + title + "</b></div>";
			var coords = results.features[i].coordinates;
			var latLng = new google.maps.LatLng(coords[0],coords[1]);

			var marker = new google.maps.Marker({
				position: latLng,
				map: americaMap,
				icon: iconBase,
				title: title,
				id: id
            });

            
            google.maps.event.addListener(marker,'click', (function(marker,content,infowindow) {
                return function() {
                   infowindow.setContent(content);
                   infowindow.open(americaMap, marker);
                };
            })(marker,content,infowindow));
            
        
        }
    }**/
     
    
}

function showContent(selectedId) {
	for(var i = 0; i<localResults.features.length; i++){
		var localId = localResults.features[i].id;

		if (localId == selectedId) {
				var coords = localResults.features[i].geometry.coordinates;
				var latLng = new google.maps.LatLng(coords[0],coords[1]);

				map.setZoom(14);
				map.panTo(latLng);
				break;
		}
	}
}

function addListener() {
    // Get the container element
    var container = document.getElementById("client_list");

    // Get all buttons with class="btn" inside the container
    var items = container.getElementsByClassName("item");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < items.length; i++) {
        items[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }
}

$('.carousel .vertical .item').each(function() {
	var next = $(this).next();
	if (!next.length) {
			next = $(this).siblings(':first');
	}
	next.children(':first-child').clone().appendTo($(this));

	for (var i = 1; i < 2; i++) {
			next = next.next();
			if (!next.length) {
				next = $(this).siblings(':first');
			}
        next.children(':first-child').clone().appendTo($(this));
	}
});