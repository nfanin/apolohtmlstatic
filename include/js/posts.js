var xmlhttp = new XMLHttpRequest();
var url = "../content/data/wp_posts.json";

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var productArray = JSON.parse(this.responseText);
        populateProducts(productArray);
    }
};

xmlhttp.open("GET", url, true);
xmlhttp.send();

function populateProducts(productArray) {
  var postItem = "";
  var i;

for (i = 0; i < productArray.length; i++) {
  postItem += '<!-- Post Item --><div class="isotope_item isotope_item_portfolio isotope_item_portfolio_3 isotope_column_3">';
  postItem += '<article class="post_item post_item_portfolio post_item_portfolio_3"><div class="post_content isotope_item_content ih-item colored square effect_shift left_to_right">';
  postItem += '<div class="post_featured img"><a href="post.php?id='+ productArray[i].ID +'"><img alt="" src="'+ productArray[i].image_small +'"></a></div><div class="post_info_wrap info">';
  postItem += '<div class="info-back"><h4 class="post_title"><a href="post.php?id='+ productArray[i].ID +'">'+ productArray[i].post_title +'</a></h4><div class="post_descr">';
  postItem += '<p class="post_info"><span class="post_info_item">Publicado <a href="#" class="post_info_date">'+ productArray[i].post_date +'</a></span></p>';
  postItem += '<p><a href="post.php?id='+ productArray[i].ID +'">'+ productArray[i].post_content_home +' ...</a>';
  postItem += '</p></div></div></div></div></article></div><!-- /Post Item -->';

}
  document.getElementById("posts_list").innerHTML = postItem;
}

function openPostDetail(productId) {
  window.open("../es/post.php?id='" + productId + "'","mywindow");
}
