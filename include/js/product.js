var xmlhttp = new XMLHttpRequest();
var url = "../content/data/products.json";
var productArray = [];
var productItem = "";
var selectedCategoryArray = [];
var showingProducts = 0;
var lang = getUrlVars()["lang"]; 
var totalProducts = 0
if (lang == null) {
  lang = "es";
}

xmlhttp.open("GET", url, true);
xmlhttp.send();

/*
 * This method charge and list all the products.
 */
xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        productArray = JSON.parse(this.responseText);
        populateProducts();
    }
};

/*
* This methid check if an url category comes and sets the filter.
*/
document.onreadystatechange = function() {
  if (this.readyState == "complete") {
    var category = getUrlVars()["category"];
    if (category != undefined) {
      filterProducts(category);
    }
  }
}

function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
  });
  return vars;
}

function populateProducts() {  
  totalProducts = productArray.length;
  document.getElementById("product_list_id").innerHTML = "";
  productItem = "";
  for (var i = 0; i < productArray.length; i++) {
    addCurrentProduct(productArray[i]);    
  }
  document.getElementById("product_list_id").innerHTML = productItem;
}

function filterProducts(selectedCategory) {
  showingProducts = 0;
  document.getElementById("product_list_id").innerHTML = "";
  productItem = "";
  //TODO: Improove this using filter functions.
  for (var i = 0; i < productArray.length; i++) {
    var categoryArray = productArray[i].category_filter.split(",");
    for(var j = 0; j < categoryArray.length; j++) {
      if(categoryArray[j] == selectedCategory) {
        addCurrentProduct(productArray[i]);    
        showingProducts++;
      }
    }
  }
  document.getElementById("product_list_id").innerHTML = productItem;

  updateSelectedCategoryIndex(selectedCategory);
  updateShowingResultsSection();
}

function openProductDetail(productId) {
  window.open("../es/product-detail.php?id='" + productId + "'","mywindow");
}

window.onhashchange = function() {
    var actualPage = window.location.href;
    actualPage = actualPage.split("#");
    //alert(actualPage[1]);
}

function addCurrentProduct(currentProduct) {
  var productTranslatedName = currentProduct.name[lang];
  var productName = productTranslatedName + "<br/>(" + currentProduct.caliber + ")";

  productItem += '<!-- Product Item --><li class="product column-1_3 "><div class="post_item_wrap"><div class="post_featured"><div class="post_thumb">';
  productItem += '<a href="../es/product-detail.php?id=' + currentProduct.id + '">';
  if (currentProduct.new == "true") {
    productItem += '<span class="onsale">NUEVO!</span>';
  }
  productItem += '<a href="../es/product-detail.php?id=' + currentProduct.id + '"><img src="' + currentProduct.image_small + '" alt="" title="Product" /></a>';
  productItem += '</a></div></div><div class="post_content"><h2 class="woocommerce-loop-product__title"><a id="product_name_id1" href="../es/product-detail.php?id=' + currentProduct.id + '">' + productName + '</a></h2>';
  productItem += '<a href="../es/product-detail.php?id=' + currentProduct.id + '" class="button add_to_cart_button">VER MÁS</a></div></div></li><!-- /Product Item -->';
}

function updateSelectedCategoryIndex(selectedCategory) {
  selectedCategoryArray.push(selectedCategory)
  for (let i = 0; i<selectedCategoryArray.length; i++) {
    var actualElement = document.getElementById(selectedCategoryArray[i]);
    if (actualElement.classList.contains("current-category")) {
      actualElement.classList.remove("current-category");
    }
  }
  document.getElementById(selectedCategory).classList.add("current-category");
}

function updateShowingResultsSection() {
    document.getElementById("showing_results").innerHTML = "Mostrando " + showingProducts + " de " + totalProducts + " resultados!";
}

