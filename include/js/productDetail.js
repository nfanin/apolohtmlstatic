var id = getUrlVars()["id"];
var lang = getUrlVars()["lang"]; 
if (lang == null) {
  lang = "es";
}
var xmlhttp = new XMLHttpRequest();
var url = "../content/data/products.json";
var productObject;

xmlhttp.open("GET", url, true);
xmlhttp.send();

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      //This should be like a hashmap.
      var productObjectArray = JSON.parse(this.responseText);
    }
    for (var i in productObjectArray) {
      if (productObjectArray[i].id == id) {
        productObject = productObjectArray[i];
      }
    }
    populateData();
    loadImage();
};


function populateData() {
  if(productObject == null) {
    return;
  }
  var productName = productObject.name[lang];
  var productEspecification = productObject.especification[lang];
  var productPresentation = productObject.presentation[lang];

   document.getElementById("pellet_category").innerHTML = productObject.category;
   document.getElementById("pellet_name_category").innerHTML = productName;
   document.getElementById("name_product_detail").innerHTML = productName;
   document.getElementById("caliber_product_detail").innerHTML = productObject.caliber;
   document.getElementById("weight_product_detail").innerHTML = productObject.weight;
   document.getElementById("especification_product_detail").innerHTML = productEspecification;
   document.getElementById("presentation_product_detail").innerHTML = productPresentation;
   document.getElementById("categories_product_detail").innerHTML = productObject.category + ", " + productObject.caliber;
   document.getElementById("product_detail_id").innerHTML = productObject.product_id;
}

function loadImage() {
  if (productObject == null) {
    return;
  }
  var mainImage = new Image;
  mainImage.src = productObject.image_main;
  var image_product_detail = document.getElementById('image_product_detail');
  mainImage.onload = function() {
    image_product_detail.src = this.src;
  }
}
