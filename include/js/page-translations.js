var linguJSON = {
    "languages": [
        {
            "lang_name": "Español",
            "lang_code": "es",
            "url_pattern": "?"
        },
        {
            "lang_name": "English",
            "lang_code": "en",
            "url_pattern": "?"
        }
    ],
    "translated_segments": [
        {
            "source": "BALINES",
            "target_en": "PELLETS",
            "target_pt": "Pellets"
        },
        {
            "source": "EMPRESA",
            "target_en": "COMPANY",
            "target_pt": "Pellets"
        },
        {
            "source": "CERTIFICACIONES",
            "target_en": "CERTIFICATIONS",
            "target_pt": "Pellets"
        },
        {
            "source": "NOVEDADES",
            "target_en": "NEWS",
            "target_pt": "Pellets"
        },
        {
            "source": "CONTACTO",
            "target_en": "CONTACT US",
            "target_pt": "Pellets"
        },
        {
            "source": "COMPRA AHORA!",
            "target_en": "BUY NOW!",
            "target_pt": "BUY NOW!"
        },
        {
            "source": "Visitá nuestra tienda on-line",
            "target_en": "Visit our on-line store",
            "target_pt": "Visit our on-line store"
        },
        {
            "source": "CATALOGO BALINES",
            "target_en": "DOWNLOAD AVAILABLE PELLETS",
            "target_pt": "BUY NOW!"
        },
        {
            "source": "CATALOGO EXPLORER",
            "target_en": "DOWNLOAD EXPLORER GROUP CATALOG",
            "target_pt": "BUY NOW!"
        },
        {
            "source": "CATALOGO RIFLES",
            "target_en": "DOWNLOAD ARMS AND ACCESORIES CATALOG",
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'PRODUCTOS',
            "target_en": 'FEATURED ',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'DESTACADOS',
            "target_en": 'PRODUCTS ',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'ENCUENTRA LAS MEJORES OPCIONES EN BALINES',
            "target_en": 'FIND THE BEST OPTIONS IN PELLETS',
            "target_pt": "BUY NOW!"
        },
        {
            "source": '<b>CERTIFICACIÓN NORMAS IRAM</b>',
            "target_en": 'IRAM STANDARD CERTIFICATION',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'ISO DE CALIDAD 9001',
            "target_en": 'ISO QUALITY 9001',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'MEDIO AMBIENTE 14001',
            "target_en": 'ENVIRONMENT 14001',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'MÁS INFORMACIÓN',
            "target_en": 'MORE INFORMATION',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'VER MÁS',
            "target_en": 'VIEW MORE',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'CATEGORÍAS',
            "target_en": 'CATEGORIES',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'NUEVO!',
            "target_en": 'NEW!',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'BALINES AIR BOSS',
            "target_en": 'AIR BOSS PELLETS',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'BALINES PREMIUM',
            "target_en": 'PREMIUM PELLETS',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'BALINES ESTANDAR',
            "target_en": 'STANDARD PELLETS',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'BALINES POR CALIBRE',
            "target_en": 'PELLETS BY CALIBER',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'ENCONTRA NUESTROS PRODUCTOS EN LOS SIGUIENTES PUNTOS DE VENTA Y PAÍSES:',
            "target_en": 'FIND OUR PRODUCTS AT THE FOLLOWING POINTS OF SALE AND COUNTRIES:',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'AMÉRICA',
            "target_en": 'AMERICA',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'EUROPA',
            "target_en": 'EUROPE',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'OCEANÍA',
            "target_en": 'OCEANIA',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'BIENVENIDO',
            "target_en": 'WELCOME',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'A BALINES APOLO',
            "target_en": 'TO BALINES APOLO',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Fabrica De balines Apolo es una empresa que desde el año 2001 se encuentra en constante Evolución e Innovación… siendo una de las pocas en el rubro que ha logrado Certificaciones de calidad y medio Ambiente a nivel Internacional.( 14001 y 9001).',
            "target_en": 'Apolo Air guns Factory, is a company that since 2001 is in constant evolution and innovation …. Being one of the few in the field that has achieved Quality Certifications and Environment at International level. (14001 and 9001)',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'En la actualidad, se fabrican 3 Líneas de Balines de Plomo para Armas de  Aire Comprimidos y 1 línea de fabricación de balines de Acero cobreados (BBs).',
            "target_en": 'At present, 3 Lines of Air Guns Pellets for Compressed Air Weapons and 1 line of manufacture of Copper Steel Balines (BBs) are manufactured.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Además, contamos con sistema de baño de Cobre especial, dado que el mismo no se oxida y con el tiempo sigue teniendo el mismo brillo y excelente presentación en los balines, con este recubrimiento hemos logrado en algunos modelos de balines hasta un 20% mas de Velocidad.',
            "target_en": 'In addition, we have a special copper bath system, since it does not oxidize and over time still has the same brightness and excellent presentation in the air guns pellets, with this coating we have achieved in some models of pellets up to 20% more than Speed.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'NUESTRAS',
            "target_en": 'OUR',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'LÍNEAS DE PRDOUCCIÓN',
            "target_en": 'PRODUCTION LINES',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Línea Estandar o Conic',
            "target_en": 'Standard or Conic line',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Estos balines tienen una excelente relación calidad-precio para el divertimento o plinking.',
            "target_en": 'Whose pellets have an excellent value for money for diverting or plinking',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Línea Premium',
            "target_en": 'Premium Line',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'La segunda Línea denominada Premium, son balines estampados de Alta Gama, de diferentes calibres, pesos y puntas. Pudiendo abarcar casi todo el abanico de posibilidad que un amante a la caza o al tiro al Blanco puede necesitar.',
            "target_en": 'The second line name Premium, are high-end printed pellets, of different calibers, weights and tips. Which can cover almost the whole range of possibilities that a lover hunting or target shooting may need.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Línea AIR BOSS',
            "target_en": 'AIR BOSS Line',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Y nuestra ultima Línea, denomina AIR BOSS, si bien también son estampados, los mismos son seleccionados por una Línea de inspección automática con Laser infrarojo, teniendo dicha medición una tolerancia de 0.004mm. por lo que hace que sea unos de los balines más Precisos del mercado, teniendo con ellos excelentes agrupaciones tanto en Competencia como en cacería con los nuevos Rifles de PCP.',
            "target_en": 'Our last line, name AIR BOSS, although they are also printed air guns pellets, they are selected by an automatic inspection line with red infrared Laser, this measurement having a tolerance of 0.004mm. so it makes it one of the Air Guns Pellets More accurate market, having excellent groupings in both Competition and Hunting with the new PCP Rifles.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'NUESTROS NÚMEROS',
            "target_en": 'OUR NUMBERS',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'BALINES POR MES',
            "target_en": 'PELLETS BY MONTH',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'LATAS POR DÍA',
            "target_en": 'TINS PER DAY',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'PAISES',
            "target_en": 'COUNTRIES',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'AÑOS DE TRAYECTORIA',
            "target_en": 'EXPERIENCE DAYS',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'H',
            "target_en": 'T',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'oy en Apolo disponemos de un sistema de gestión de calidad, cumplimos con las exigencias comerciales y sociales; nos proponemos objetivos y metas ambientales.',
            "target_en": 'oday in Apolo we have a quality management system, we comply with commercial and social demands; we propose environmental objectives and goals.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Durante muchos años, en Apolo, trabajamos para lograr la máxima calidad en nuestra fabrica y de esa manera obtener excelencia en nuestros productos.',
            "target_en": 'For many years, in Apolo, we work to achieve the highest quality in our factory and in this way obtain excellence in our products.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Sostenemos que el cuidado del medio ambiente esta estrechamente vinculado a las metas que tienen que ver con el crecimiento',
            "target_en": 'We hold that the care of the environment is closely linked to the goals that have to do with the growth.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Mantenemos una política de responsabilidad y preocupación respetando el medio ambiente y tratando a diario de contribuir de forma voluntaria a la mejora ambiental y con ello, nuestra competitividad en el mercado.',
            "target_en": 'We maintain a policy of responsibility and concern respecting the environment and daily trying to contribute voluntarily to environmental improvement and with it, our competitiveness in the market.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'En reconocimiento de nuestro accionar, hemos recibido la CERTIFICACIÓN INTERNACIONAL DE LA NORMA ISO MEDIO AMBIENTE 14001 Y LA CERTIFICACIÓN DE IRAM ISO 9001: 2015.',
            "target_en": 'In recognition of our actions, we have received the INTERNATIONAL CERTIFICATION OF ISO ENVIRONMENT STANDARD 14001 AND THE CERTIFICATION OF IRAM ISO 9001: 2015.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'CONTACTANOS',
            "target_en": 'CONTACT US',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'HOY!',
            "target_en": 'TODAY!',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'SU CORREO ELECTRONICO NO SERA PUBLICADO.',
            "target_en": 'YOUR EMAIL ADDRESS WILL NOT BE PUBLISHED.',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'LOS CAMPOS REQUERIDOS SON MARCADOS CON *',
            "target_en": 'REQUIRED FIELDS ARE MARKED WITH *',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Dirección',
            "target_en": 'Address',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Teléfono',
            "target_en": 'Phone',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Nombre',
            "target_en": 'Name',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Asunto',
            "target_en": 'Subject',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Mensaje',
            "target_en": 'Message',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Enviar Mensaje',
            "target_en": 'Send Message',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'ESPECIFICACIONES',
            "target_en": 'SPECIFICATIONS',
            "target_pt": "BUY NOW!"
        },
        {
            "source": '<b>Calibre:</b>',
            "target_en": '<b>Caliber:</b>',
            "target_pt": "BUY NOW!"
        },
        {
            "source": '<b>Peso:</b>',
            "target_en": '<b>Weight:</b>',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'Categorias:',
            "target_en": 'Categories:',
            "target_pt": "BUY NOW!"
        },
        {
            "source": 'ID de Producto:',
            "target_en": 'Product ID:',
            "target_pt": "BUY NOW!"
        }
    ]
};