<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Certificaciones</title>

        <?php include 'views/libs/header_includes.php'; ?>

    </head>

    <body class="page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/section_header.php'; ?>
                <!-- /Header Mobile -->
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">CERTIFICACIONES</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.php">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span class="breadcrumbs_item current">CERTIFICACIONES</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_no">
                    <!-- Content -->
                    <div class="content">
                        <article class="post_item post_item_single">
                            <section class="post_content">
                                <!-- Welcome to our store -->
                                <div class="empty_space height_4_8em"></div>
                                <div class="sc_section">
                                    <div class="content_wrap">
                                      <!-- Main Image -->
                                      <div class="empty_space height_2_65em"></div>
                                      <figure class="sc_image sc_image_shape_square">
                                          <img src="../content/image/compromiso_certificado-1.png" alt="" />
                                      </figure>
                                      <div class="empty_space height_2_65em"></div>
                                      <!-- /Main Image -->
                                        <div class="sc_section_inner">
                                          <div class="sc_dropcaps sc_dropcaps_style_3">
                                            <span class="sc_dropcaps_item">H</span>
                                            <span style="text-align: justify; font-size: 18px; color: #fff; line-height: 1.5em;">
                                              oy en Apolo disponemos de un sistema de gestión de calidad, cumplimos con las exigencias comerciales y sociales; nos proponemos objetivos y metas ambientales.
                                            </span>
                                          </div>
                                          <div class="empty_space height_2_65em"></div>
                                          <div class="wpb_text_column wpb_content_element  vc_custom_1526229317174" >
                                            <div class="wpb_wrapper">
                                              <blockquote style="background-color: #ffcc29;">
                                                <p style="text-align: justify; color: #000;">
                                                  Durante muchos años, en Apolo, trabajamos para lograr la máxima calidad en nuestra fabrica y de esa manera obtener excelencia en nuestros productos.</p>
                                                <p style="text-align: justify; color: #000;">
                                                  Sostenemos que el cuidado del medio ambiente esta estrechamente vinculado a las metas que tienen que ver con el crecimiento</p>
                                                </blockquote>
                                              </div>
                                            </div>

                                            <figure class="sc_image  alignleft sc_image_shape_square margin_top_null" style="width:40%;"><img src="http://localhost/apolo/wp-content/uploads/2018/04/DSC04980-1-e1533074233611.jpg" alt="" /></figure>
	                                           <div class="wpb_text_column wpb_content_element " >
		                                             <div class="wpb_wrapper">
			                                                <p style="text-align: justify; font-size: 18px; line-height: 1.5em; color: #fff;">Mantenemos una política de responsabilidad y preocupación respetando el medio ambiente y tratando a diario de contribuir de forma voluntaria a la mejora ambiental y con ello, nuestra competitividad en el mercado.</p>
                                                      <p style="text-align: justify; font-size: 18px; line-height: 1.5em; color: #fff;">En reconocimiento de nuestro accionar, hemos recibido la CERTIFICACIÓN INTERNACIONAL DE LA NORMA ISO MEDIO AMBIENTE 14001 Y LA CERTIFICACIÓN DE IRAM ISO 9001: 2015.</p>

		                                                  </div>
	                                            </div>

                                            <div class="empty_space height_5_7em"></div>
                                            <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2 responsive_columns margin_top_medium margin_bottom_medium">
                                                <div class="column-1_2 sc_column_item sc_column_item_1 odd first">
                                                  <figure class="sc_image sc_image_shape_square">
                                                      <img src="../content/image/9001.png" alt="" />
                                                  </figure>
                                                </div><div class="column-1_2 sc_column_item sc_column_item_2 even">
                                                    <figure class="sc_image sc_image_shape_square">
                                                        <img src="../content/image/14001.png" alt="" />
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="empty_space height_3_9em"></div>
                            </section>
                        </article>
                    </div>
                </div>
                <!-- /Page Content -->

                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>

    </body>

</html>
