<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Novedades</title>

        <?php include 'views/libs/header_includes.php'; ?>

        <link rel="stylesheet" href="../include/css/core.portfolio.css" type="text/css" media="all" />

    </head>

    <body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/section_header.php'; ?>
                <!-- /Header Mobile -->
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">NOVEDADES</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.html">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span class="breadcrumbs_item current">NOVEDADES</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_yes">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content">
                            <div class="isotope_wrap" data-columns="3">

                              <!-- Products List -->
                              <div id="posts_list">
                              </div>
                              <!-- /Products List -->
                            </div>
                            <!-- Pagination -->
                            <!--<nav id="pagination" class="pagination_wrap pagination_pages">
                                <span class="pager_current active ">1</span>
                                <a href="#" class="">2</a>
                                <a href="#" class="pager_next ">&#8250;</a>
                                <a href="#" class="pager_last ">&raquo;</a>
                            </nav>-->
                            <!-- /Pagination -->
                        </div>
                        <!-- /Content -->
                    </div>
                    <!-- </div> class="content_wrap"> -->
                </div>
                <!-- /Page Content -->
                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>

        <script type="text/javascript" src="../include/js/vendor/isotope/dist/isotope.pkgd.min.js"></script>
        <script type="text/javascript" src="../include/js/hover/jquery.hoverdir.min.js"></script>

        <script type="text/javascript" src="../include/js/posts.js"></script>


    </body>

</html>
