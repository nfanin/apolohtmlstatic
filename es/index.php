<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta name="format-detection" content="telephone=no">

      <title>Balines Apolo</title>

      <?php include 'views/libs/header_includes.php';?>

    </head>
    <body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/header.php';?>                
                <!-- /Header -->
                <!-- Page Content -->
                <section class="slider_wrap slider_fullwide slider_engine_revo slider_alias_home-4">
                    <div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery">
                      <!-- VIMEO VIDEO PLAYER-->
                      <div class="sc_video_player">
                          <div class="sc_video_frame sc_video_play_button hover_icon hover_icon_play" data-width="100%" data-height="659" data-video="&lt;iframe class=&quot;video_frame&quot;
                          src=&quot;https://player.vimeo.com/video/381320004?autoplay=1&quot; width=&quot;100%&quot; height=&quot;659&quot; frameborder=&quot;0&quot; webkitAllowFullScreen=&quot;webkitAllowFullScreen&quot; mozallowfullscreen=&quot;mozallowfullscreen&quot; allowFullScreen=&quot;allowFullScreen&quot;&gt;&lt;/iframe&gt;">
                              <img alt="" src="../content/image/BalinesApoloCaptura-1024x788.png">
                          </div>
                      </div>
                      <!-- /VIMEO VIDEO PLAYER-->
                    </div>
                    <!-- END REVOLUTION SLIDER -->
                </section>
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_no">
                    <!-- Content -->
                    <div class="content">
                        <article class="post_item post_item_single">
                            <section class="post_content">

                                <!-- Product categories -->
                                <div class="bg_dark_style_2">
                                    <div class="content_wrap">
                                        <div class="empty_space height_2_8em"></div>
                                        <div class="woocommerce columns-3">
                                            <ul class="products">
                                                <li class="product-category product first">
                                                    <div class="post_item_wrap">
                                                        <div class="post_featured">
                                                            <div class="post_thumb">
                                                                <a href="../content/download/CatalogoBalines2021.pdf">
                                                                    <img src="../content/image/catalogo_balines.png" alt="" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_content">
                                                            <h2 class="woocommerce-loop-category__title">
                                                                <a href="../content/download/CatalogoBalines2019.pdf">CATALOGO BALINES</a>
                                                                <mark class="count"></mark>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="product-category product">
                                                    <div class="post_item_wrap">
                                                        <div class="post_featured">
                                                            <div class="post_thumb">
                                                                <a href="../content/download/CatalogoExplorer2019.pdf">
                                                                    <img src="../content/image/catalogo_explorer.png" alt="" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_content">
                                                            <h2 class="woocommerce-loop-category__title">
                                                                <a href="../content/download/CatalogoExplorer2019.pdf">CATALOGO EXPLORER</a>
                                                                <mark class="count"></mark>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="product-category product">
                                                    <div class="post_item_wrap">
                                                        <div class="post_featured">
                                                            <div class="post_thumb">
                                                                <a href="../content/download/CatalogoRifles2019.pdf">
                                                                    <img src="../content/image/catalogo_rifles.png" alt="" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="post_content">
                                                            <h2 class="woocommerce-loop-category__title">
                                                                <a href="../content/download/CatalogoRifles2019.pdf">CATALOGO RIFLES</a>
                                                                <mark class="count"></mark>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="empty_space height_0_7em"></div>
                                    </div>
                                </div>
                                <!-- /Product categories -->
                                <!-- Featured Products -->
                                <div class="custom_texture_bg1">
                                    <div class="content_wrap">
                                        <div class="empty_space height_5_5em"></div>
                                        <div class="sc_section scheme_light">
                                            <div class="sc_section_inner">
                                                <h2 class="sc_section_title sc_item_title">PRODUCTOS <span class="thin">DESTACADOS</span></h2>
                                                <div class="sc_section_descr sc_item_descr">
                                                    ENCUENTRA LAS MEJORES OPCIONES EN BALINES
                                                </div>
                                                <div class="woocommerce columns-4">
                                                    <ul class="products">
                                                      <!-- Products List -->
                                                      <ul id="product_list_id" class="products">
                                                      </ul>
                                                      <!-- /Products List -->
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="empty_space height_5_7em"></div>
                                    </div>
                                </div>
                                <!-- /Featured Products -->

                                <!-- Popular News -->
                                <div class="empty_space height_6_6em"></div>
                                <div class="content_wrap">
                                    <div class="widget_area sc_recent_news_wrap">
                                        <aside class="widget widget_recent_news">
                                            <div class="sc_recent_news sc_recent_news_style_news-magazine sc_recent_news_with_accented">
                                                <div class="sc_recent_news_header sc_recent_news_header_split">
                                                    <div class="sc_recent_news_header_captions">
                                                        <h3 class="sc_recent_news_title">Novedades
                                                            <span class="thin"> Recientes</span>
                                                        </h3>
                                                    </div><div class="sc_recent_news_header_categories">
                                                        <!--<a href="blog-classic.html" class="sc_recent_news_header_category_item">All News</a>
                                                        <a href="certificate-single.html" class="sc_recent_news_header_category_item">Certificates</a>
                                                        <a href="gallery-grid.html" class="sc_recent_news_header_category_item">Gallery</a>
                                                        <span class="sc_recent_news_header_category_item sc_recent_news_header_category_item_more">More
                                                            <span class="sc_recent_news_header_more_categories">
                                                                <a href="blog-masonry-2-columns.html" class="sc_recent_news_header_category_item">Masonry (2 columns)</a>
                                                                <a href="blog-masonry-3-columns.html" class="sc_recent_news_header_category_item">Masonry (3 columns)</a>
                                                                <a href="blog-portfolio-2-columns.html" class="sc_recent_news_header_category_item">Portfolio (2 columns)</a>
                                                                <a href="blog-masonry-3-columns.html" class="sc_recent_news_header_category_item">Portfolio (3 columns)</a>
                                                                <a href="post-formats.html" class="sc_recent_news_header_category_item">Post Formats</a>
                                                            </span>
                                                        </span>-->
                                                    </div>
                                                </div>
                                                <div class="columns_wrap">
                                                    <div class="column-1_2">
                                                        <!-- Post Item -->
                                                        <article class="post_item post_accented_on">
                                                            <div class="post_featured">
                                                                <div class="post_thumb">
                                                                    <a id="mainPostImageLink" class="hover_icon hover_icon_link" href="post-single.html">
                                                                        <img id="mainPostImage" alt="" src="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="post_header">
                                                                <h5 class="post_title">
                                                                    <a id="mainPostTitle" href="post-single.html"></a>
                                                                </h5>
                                                                <div class="post_meta">
                                                                    <span class="post_meta_date">Publicado el
                                                                        <a id="mainPostDate" href="#">February 3, 2017</a>
                                                                    </span>
                                                                    <span class="post_meta_author">Por Balines Apolo</span>
                                                                </div>
                                                            </div>
                                                            <div class="post_content">
                                                                <p id ="mainPostDescription"></p>
                                                            </div>
                                                            <div class="post_footer">
                                                                <a id="mainPostLink" class="sc_button sc_button_style_dark" href="post-single.html">Ver más</a>
                                                            </div>
                                                        </article>
                                                    </div><div id="mainPostItems" class="column-1_2">
                                                        <!-- Post Item -->
                                                       
                                                        <article class="post_item post_accented_off">
                                                            <div class="post_featured">
                                                                <div class="post_thumb">
                                                                    <a class="hover_icon hover_icon_link" href="post-single.html">
                                                                        <img alt="Rental Firearms &#038; Fees" src="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="post_header">
                                                                <h6 class="post_title">
                                                                    <a href="post-single.html">Rental Firearms &#038; Fees</a>
                                                                </h6>
                                                                <div class="post_meta">
                                                                    <span class="post_meta_date">Posted
                                                                        <a href="#">February 3, 2017</a>
                                                                    </span>
                                                                    <span class="post_meta_author">By Jack Black</span>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <!-- /Post Item -->
                                                        <!-- Post Item -->
                                                        <article class="post_item post_accented_off">
                                                            <div class="post_featured">
                                                                <div class="post_thumb" data-image="http://placehold.it/2400x1600.jpg" data-title="Gun Range">
                                                                    <a class="hover_icon hover_icon_link" href="post-single.html">
                                                                        <img alt="" src="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="post_header">
                                                                <h6 class="post_title">
                                                                    <a href="post-single.html">Gun Range</a>
                                                                </h6>
                                                                <div class="post_meta">
                                                                    <span class="post_meta_date">Posted
                                                                        <a href="#">February 3, 2017</a>
                                                                    </span>
                                                                    <span class="post_meta_author">By Jack Black</span>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <!-- /Post Item -->
                                                        <!-- Post Item -->
                                                        <article class="post_item post_accented_off">
                                                            <div class="post_featured">
                                                                <div class="post_thumb">
                                                                    <a class="hover_icon hover_icon_link" href="post-single.html">
                                                                        <img alt="" src="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="post_header">
                                                                <h6 class="post_title">
                                                                    <a href="post-single.html">Firearms Classes and Registration</a>
                                                                </h6>
                                                                <div class="post_meta">
                                                                    <span class="post_meta_date">Posted
                                                                        <a href="post-single.html">February 3, 2017</a>
                                                                    </span>
                                                                    <span class="post_meta_author">By Jack Black</span>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <!-- /Post Item -->
                                                        <!-- Post Item -->
                                                        <article class="post_item post_accented_off">
                                                            <div class="post_featured">
                                                                <div class="post_thumb">
                                                                    <a class="hover_icon hover_icon_link" href="post-single.html">
                                                                        <img alt="" src="">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="post_header">
                                                                <h6 class="post_title">
                                                                    <a href="post-single.html">How To Buy And Register A Suppressor</a>
                                                                </h6>
                                                                <div class="post_meta">
                                                                    <span class="post_meta_date">Posted
                                                                        <a href="#">February 2, 2017</a>
                                                                    </span>
                                                                    <span class="post_meta_author">By Jack Black</span>
                                                                </div>
                                                            </div>
                                                        </article>
                                                        <!-- /Post Item -->
                                                    </div>
                                                </div>
                                            </div>
                                        </aside>
                                    </div>
                                </div>
                                <div class="empty_space height_7_8em"></div>
                                <!-- /Popular News -->




                                <!-- Call To Action -->
                                <div class="accent2_bg scheme_light">

                                    <div class="sc_call_to_action sc_call_to_action_style_1 sc_call_to_action_align_center">
                                      <div class="sc_call_to_action_info">
                                            <div class="empty_space height_5_7em"></div>
                                            <img src="../content/image/apolo_certifications.jpg" alt="" title=""/>
                                            <div class="empty_space height_5_7em"></div>
                                            <h2 class="sc_call_to_action_title sc_item_title"><b>CERTIFICACIÓN NORMAS IRAM</b></h2>
                                            <div class="sc_call_to_action_descr sc_item_descr">
                                              ISO DE CALIDAD 9001<br/>
                                              MEDIO AMBIENTE 14001</div>
                                            <div class="sc_call_to_action_buttons sc_item_buttons">
                                                <div class="sc_call_to_action_button sc_item_button">
                                                    <a href="certifications.php" class="sc_button sc_button_square sc_button_style_dark sc_button_size_small">MÁS INFORMACIÓN</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty_space height_6em"></div>
                                </div>
                                <!-- /Call To Action -->
                            </section>
                        </article>
                    </div>
                    <!-- /Content -->
                </div>
                <!-- /Page Content -->
                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
            
        </div>
        <!-- /Body wrap -->
        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>
         
        <a href="https://www.camospa.com" id="chileButton" title="Chile">
        <i class="demo-icon icon-megaphone-light"><b>APOLO<br/>CHILE</b></i></a>
        <?php include 'views/libs/footer_includes.php'; ?>
        <script type="text/javascript" src="../include/js/product_main_list.js"></script>

    </body>
</html>
