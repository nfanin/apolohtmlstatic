<!-- Footer 2-->
<footer class="contacts_wrap scheme_original">
    <div class="contacts_wrap_inner">
        <div class="content_wrap">
            <!-- Logo -->
            <div class="logo">
                <a href="index.html">
                    <img src="../content/image/ApoloLogo.png" class="logo_footer" alt="">
                </a>
            </div>
            <!-- /Logo -->
            <!-- Contact Adress -->
            <div class="contacts_address">
                <address class="address_right">
                    <p>Teléfono +54-3537-430 320</p>
                    <p>Móvil: +54-3537-666 175</p>
                </address>
                <address class="address_left">
                    <p>Av. Int. Pedro Paoloni 1546 - Parque Industrial</p>
                    <p>2553 - Justiniano Posse, Córdoba, Argentina</p>
                </address>
            </div>
            <!-- /Contact Adress -->
            <!-- Socials -->
            <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_medium">
                <div class="sc_socials_item">
                    <a href="https://www.facebook.com/BalinesApolo" target="_blank" class="social_icons social_facebook">
                        <span class="icon-facebook"></span>
                    </a>
                </div>
                <div class="sc_socials_item">
                    <a href="https://twitter.com/apolobalines" target="_blank" class="social_icons social_twitter">
                        <span class="icon-twitter"></span>
                    </a>
                </div>
                <div class="sc_socials_item">
                    <a href="https://www.instagram.com/balines.apolo/" target="_blank" class="social_icons social_instagram">
                        <span class="icon-instagramm"></span>
                    </a>
                </div>
            </div>
            <!-- /Socials -->
        </div>
    </div>
</footer>
<!-- /Footer 2-->
<!-- Certifications footer -->
<footer class="footer_wrap widget_area scheme_original">
     <div class="footer_wrap_inner widget_area_inner">
       <div class="content_wrap">
           <div class="columns_wrap">
              <aside id="media_image-1" class="widget_number_1 column-1_7 widget widget_media_image">
                <img width="104" height="80" src="../content/image/logo_bajo.png" class="image wp-image-1644  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" />
              </aside>
              <aside id="media_image-2" class="widget_number_2 column-1_7 widget widget_media_image">
                <a href="http://www.cacec.com.ar/">
                  <img width="300" height="99" src="../content/image/logo-cacec-300x99.png" class="image wp-image-1648  attachment-medium size-medium" alt="" style="max-width: 100%; height: auto;"
                  srcset="../content/image/logo-cacec-300x99.png 300w, ../content/image/logo-cacec-768x254.png 768w, ../content/image/logo-cacec-600x199.png 600w, ../content/image/logo-cacec.png 876w" sizes="(max-width: 300px) 100vw, 300px" />
                </a>
              </aside>
              <aside id="media_image-3" class="widget_number_3 column-1_7 widget widget_media_image">
                <img width="77" height="125" src="../content/image/logo_iram_iso_14000.png" class="image wp-image-1647  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" />
              </aside>
              <aside id="media_image-4" class="widget_number_4 column-1_7 widget widget_media_image">
                <img width="77" height="125" src="../content/image/logo_iram_iso_9000.png" class="image wp-image-1646  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" />
              </aside>
              <aside id="media_image-5" class="widget_number_5 column-1_7 widget widget_media_image">
                <img width="77" height="152" src="../content/image/logo_exporta.png" class="image wp-image-1645  attachment-full size-full" alt="" style="max-width: 100%; height: auto;" />
              </aside>
              <aside id="media_image-6" class="widget_number_6 column-1_7 widget widget_media_image">
              <script language="JavaScript" type="text/javascript">TrustLogo("https://micuenta.donweb.com/img/sectigo_positive_sm.png", "CL1", "none");</script><a href="https://donweb.com/es-ar/certificados-ssl" id="comodoTL" title="Certificados SSL Argentina">Certificados SSL Argentina</a>
            </aside>
            </div>	<!-- /.columns_wrap -->
        </div>  <!-- /.content_wrap -->
    </div>	<!-- /.footer_wrap_inner -->
</footer>	<!-- /.footer_wrap -->

<!-- /Certification footer -->
<!-- Copyright -->
<div class="copyright_wrap copyright_style_text  scheme_original">
    <div class="copyright_wrap_inner">
        <div class="content_wrap">
          <div class="sc_infobox sc_infobox_style_info sc_infobox_closeable sc_infobox_iconed icon-info-1">
              <p>Actualmente estamos renovando nuestro sitio web, es por ello que algunas secciones pueden no funcionar como es deseado. </p>
          </div>
            <div class="copyright_text">
                <a href="#">Deon studios - Copyright Apolo 2020</a>
            </div>
        </div>
    </div>
</div>
<!-- /Copyright -->
