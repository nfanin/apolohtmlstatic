<!-- Header -->
<header class="top_panel_wrap top_panel_style_2 scheme_original">
    <div class="top_panel_wrap_inner top_panel_inner_style_2 top_panel_position_above">
      <!-- Top panel 2 -->
        <div class="top_panel_middle">
            <div class="content_wrap">
                <div class="columns_wrap columns_fluid">
                    <!-- Contacts -->
                    <div class="column-1_4 contact_field contact_phone">
                        <span class="contact_icon icon-iconmonstr-phone-2-icon"></span>
                        <span class="contact_label contact_phone">+54-3537-430 320</span>
                        <span class="contact_email">info@apolo.ar</span>
                    </div><!-- /Contacts -->
                    <!-- Logo -->
                    <div class="column-1_2 contact_logo">
                    <div class="logo">
                        <a href="https://balinesapolo.com/">
                            <img src="../content/image/ApoloLogo.png" class="logo_main" alt="">
                        </a>
                    </div>
                  </div><!-- /Logo -->
                <!-- Cart -->
                <div class="column-1_4 contact_field contact_cart">
                  <a href="https://www.apoloshop.com.ar/" target="_blank">
                    <span class="contact_icon icon-iconmonstr-shopping-cart-4-icon" style="color: #ffcc29;"></span>
                    <span class="contact_label contact_address_1" style="color: #ffcc29;"><?php echo "COMPRA AHORA!"; ?></span>
                    <span class="contact_address_2" style="color: #ffcc29;"><?php echo "Visitá nuestra tienda on-line"; ?></span>
                  </a>
                </div>
                </div>
                <!-- /Cart -->
            </div>
        </div>
        <!-- /Top panel 2 -->
        <!-- Top panel 3 -->
        <div class="top_panel_bottom">
            <div class="content_wrap clearfix">
                <?php include "navigation_menu.php"; ?>
            </div>
        </div>
        <!-- /Top panel 3 -->
    </div>
    <script type="text/javascript">const tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/"); document.write(unescape("<script src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript' %3E%3C/script%3E"));</script>

</header>
<!-- /Header -->
<?php include "navigation_mobile.php"; ?>

<script>
  document.getElementById("homeItem").classList.add("current-menu-parent");
  document.getElementById("homeItemMobile").classList.add("current-menu-parent");
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-121900043-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-121900043-1');
</script>
