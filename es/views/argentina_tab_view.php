<div class="columns_wrap">
<div class="column-2_8">
	<div class="container">
    <div class="row">

        <div class="col-md-3">
            <div id="carousel-pager" class="carousel slide " data-ride="carousel" data-interval="500000000">
                <!-- Carousel items -->
                <div class="carousel-inner vertical" id="client_list">
									<!-- Items -->
									<?php
									// Get the contents of the JSON file
									$strJsonFileContents = file_get_contents("../content/data/clients.json");
									$strJsonFileContents = str_replace("eqfeed_callback(", "", $strJsonFileContents);
									$strJsonFileContents = str_replace(");", "", $strJsonFileContents);
									$array = json_decode($strJsonFileContents, true);
									$featuresArray = $array["features"];

									for($i = 0; $i < count($featuresArray); $i++) {
										if ($i == 0) {
											echo '<div class="active item">';
										} else {
											echo '<div class="item">';
										}
										echo '<div id="'.$featuresArray[$i]["id"].'" class="post_item post_format_link" onClick="showContent('.$featuresArray[$i]["id"].')">
													<div class="post_featured" style="margin-bottom: 0em; font-style: inherit;">
															<p style="font-size: 14px;">
															'. $featuresArray[$i]["properties"]["title"] . '<br/>
																	<span class="post_info" style="text-transform: inherit;">
																	'.$featuresArray[$i]["properties"]["description"] . '</span>
															</p>
															<div class="client_separator"></div>
													</div>
											</div>
										</div>';
									}
									?>
									<!-- /Items -->
                </div>
                <div></div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-pager" role="button" data-slide="prev">
	                    <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-pager" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
<div class="column-5_8" id="argentinaMap"></div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script>

</script>