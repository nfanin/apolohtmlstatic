<!-- Menu -->
<nav class="menu_main_nav_area">
    <ul class="menu_main_nav">
        <!-- Home current-menu-ancestor current-menu-parent -->
        <li id="homeItem" class="menu-item">
            <a href="index.php">HOME</a>
        </li>
        <!-- /Home -->
        <!-- Pages -->
        <li id="shopItem" class="menu-item menu-item-has-children">
            <a href="shop.php">BALINES</a>
            <ul class="sub-menu">
                <li class="menu-item">
                    <a href="shop.php?category=Slug">SLUG</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=SteelCopper">BBS STEEL COOPER</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=AirBoss">AIR BOSS</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=Premium">PREMIUM</a>
                </li>
                <li class="menu-item">
                    <a href="shop.php?category=Estandar">ESTANDARD</a>
                </li>
            </ul>
        </li>
        <!-- /Pages -->
        <!-- Products -->
        <li id="dealersItem" class="menu-item">
            <a href="dealers.php">DEALERS</a>
        </li>
        <!-- /Products -->
        <!-- Promotion -->
        <li id="aboutUsItem" class="menu-item">
            <a href="about-us.php">EMPRESA</a>
        </li>
        <!-- /Promotion -->
        <!-- Promotion -->
        <li id="certificationsItem" class="menu-item">
            <a href="certifications.php">CERTIFICACIONES</a>
        </li>
        <!-- /Promotion -->
        <!-- Blog -->
        <li id="newsItem" class="menu-item">
            <a href="blog.php">NOVEDADES</a>
        </li>
        <!-- /Blog -->
        <!-- Contact Us -->
        <li id="contactUsItem" class="menu-item">
            <a href="contact-us.php">CONTACTO</a>
        </li>

        <li id="menu-item-1969" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1969">
            <a href="https://www.facebook.com/BalinesApolo">
                <i class="icon-facebook" style="font-size:18px;"></i>
            </a>
        </li>
        <li id="menu-item-1970" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1970">
            <a href="https://twitter.com/apolobalines">
                <span class="icon-twitter" style="font-size:18px;"></span>
            </a>
        </li>
        <li id="menu-item-2700" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2700">
            <a href="https://www.instagram.com/balines.apolo/">
                <i class="icon-instagramm" style="font-size:18px;"></i>
            </a>
        </li>    
        <!--<li id="flagItem" class="menu-item menu-item-has-children">
            <a id="es" href="" ><img src="../content/image/flags/ar.alt.png" /></a>
            <ul class="sub-menu">
                <li class="menu-item flags">
                    <a id="en" href=""><img src="../content/image/flags/us.png" /> INGLES</a>
                </li>
                <li class="menu-item flags">
                    <a id="pt" href="" ><img src="../content/image/flags/pt-pt.png" /> PORTUGUES</a>
                </li>
            </ul>
        </li>-->    
        </ul>
    </ul>
</nav>
<!-- /Menu -->
