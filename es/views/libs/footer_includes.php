<script type="text/javascript" src="../include/js/jquery/jquery.js"></script>
<script type="text/javascript" src="../include/js/jquery/jquery-migrate.min.js"></script>

<script type="text/javascript" src="../include/js/_main.js"></script>
<script type="text/javascript" src="../include/js/trx_utils.min.js"></script>
<script type="text/javascript" src="../include/js/_packed.js"></script>

<script type="text/javascript" src="../include/js/vendor/essential-grid/js/lightbox.js"></script>
<script type="text/javascript" src="../include/js/vendor/essential-grid/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="../include/js/vendor/revslider/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="../include/js/vendor/revslider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="../include/js/vendor/revslider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="../include/js/vendor/revslider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="../include/js/vendor/revslider/js/extensions/revolution.extension.navigation.min.js"></script>

<script type="text/javascript" src="../include/js/tpl-revslider-general.js"></script>
<script type="text/javascript" src="../include/js/tpl-revslider-1.js"></script>

<script type="text/javascript" src="../include/js/vendor/photostack/modernizr.min.js"></script>
<script type="text/javascript" src="../include/js/vendor/superfish.js"></script>

<script type="text/javascript" src="../include/js/utils.js"></script>
<script type="text/javascript" src="../include/js/core.init.js"></script>
<script type="text/javascript" src="../include/js/init.js"></script>

<script type="text/javascript" src="../include/js/shortcodes.js"></script>
<script type="text/javascript" src="../include/js/messages.js"></script>
<script type="text/javascript" src="../include/js/vendor/magnific/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="../include/js/vendor/boostrapslider/carousel.js"></script>
<!--<script type="text/javascript" src="../include/js/vendor/swiper/swiper.js"></script>-->

<script type="text/javascript" src="../include/js/page-translations.js"></script>
<script type="text/javascript" src="../include/js/lingumania.js"></script>