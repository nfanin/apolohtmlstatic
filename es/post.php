<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Publicación</title>

        <?php include 'views/libs/header_includes.php'; ?>

    </head>

    <body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_show sidebar_right">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">

              <!-- Header -->
              <?php include 'views/section_header.php'; ?>
              <!-- /Header -->

                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 id="post_title" class="page_title">10 Great Tactical Clothing Options</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.html">Home</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <a class="breadcrumbs_item cat_post" href="#">Post Category</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span  class="breadcrumbs_item current"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_yes">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content">
                            <article class="post_item post_item_single">
                                <!-- Featured Image -->
                                <section class="post_featured">
                                    <div class="post_thumb">
                                        <a id="image_product_detail_expand" class="hover_icon hover_icon_view" href="" title="">
                                            <img id="image_product_detail" alt="" src="">
                                        </a>
                                    </div>
                                </section>
                                <!-- /Featured Image -->
                                <!-- Post Content -->
                                <section class="post_content">
                                    <div class="post_info">
                                        <span class="post_info_item post_info_posted">Publicado
                                            <a id="post_published_date" href="#" class="post_info_date date">August 5, 2017</a>
                                        </span>
                                        <span class="post_info_item">por
                                            <a href="#" class="post_info_author">Balines Apolo</a>
                                        </span>
                                    </div>
                                    <p id="post_content"></p>

                                </section>
                                <!-- /Post Content -->

                                <!-- Gallery -->
                                <section class="post_content">
                                  <div id="image_gallery_div" class="gallery gallery-columns-3">

                                </div>
                                </section>
                                <!-- Gallery ends -->

                                <!-- Post Author -->
                                <section style="padding: -5em 1.85em; overflow: hidden; width:100%;">
                                </section>
                                <!-- /Post Author -->
                                <!-- Related Posts -->
                                <!--<section class="related_wrap">
                                    <h3 class="section_title">Artículos Relacionados</h3>
                                    <div class="columns_wrap">
                                        <div class="column-1_2 column_padding_bottom">
                                            <article class="post_item post_item_related post_item_1">
                                                <div class="post_content">
                                                    <div class="post_featured">
                                                        <div class="post_thumb">
                                                            <a class="hover_icon hover_icon_link" href="post-single.html">
                                                                <img alt="" src="http://placehold.it/370x370.jpg">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post_content_wrap">
                                                        <h5 class="post_title">
                                                            <a href="post-single.html">Great New Handguns for 2017</a>
                                                        </h5>
                                                        <div class="post_info post_info_tags">
                                                            <a class="post_tag_link" href="#">archery</a>,
                                                            <a class="post_tag_link" href="#">camping</a>,
                                                            <a class="post_tag_link" href="#">protection</a>,
                                                            <a class="post_tag_link" href="#">tactical</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div><div class="column-1_2 column_padding_bottom">
                                            <article class="post_item post_item_related post_item_2">
                                                <div class="post_content">
                                                    <div class="post_featured">
                                                        <div class="post_thumb">
                                                            <a class="hover_icon hover_icon_link" href="post-single.html">
                                                                <img alt="" src="http://placehold.it/370x370.jpg">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post_content_wrap">
                                                        <h5 class="post_title">
                                                            <a href="post-single.html">20 New 2017 Airguns</a>
                                                        </h5>
                                                        <div class="post_info post_info_tags">
                                                            <a class="post_tag_link" href="#">airguns</a>,
                                                            <a class="post_tag_link" href="#">optics</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    </div>
                                </section>-->
                                <!-- /Related Posts -->
                            </article>
                        </div>
                        <!-- /Content -->
                        <!-- Sidebar -->
                        <div class="sidebar widget_area scheme_original">
                            <div class="sidebar_inner widget_area_inner">
                                <!-- Widget: Categories -->
                                <!--<aside class="widget widget_categories">
                                    <h5 class="widget_title">Categories</h5>
                                    <ul>
                                        <li>
                                            <a href="blog-classic.html">Blog Classic</a>
                                        </li>
                                        <li>
                                            <a href="blog-masonry-2-columns.html">Masonry (2 columns)</a>
                                        </li>
                                        <li>
                                            <a href="blog-masonry-3-columns.html">Masonry (3 columns)</a>
                                        </li>
                                        <li>
                                            <a href="blog-portfolio-2-columns.html">Portfolio (2 columns)</a>
                                        </li>
                                        <li>
                                            <a href="blog-portfolio-3-columns.html">Portfolio (3 columns)</a>
                                        </li>
                                        <li>
                                            <a href="post-formats.html">Post Formats</a>
                                        </li>
                                    </ul>
                                </aside>-->
                                <!-- /Widget: Categories -->

                                <!-- Widget: Meta -->
                                <aside class="widget widget_meta">
                                    <h5 class="widget_title">Links</h5>
                                    <ul>
                                        <li>
                                            <a href="index.php">Home</a>
                                        </li>
                                        <li>
                                            <a href="shop.php"><abbr title="Mirá todos nuestros Balines">Productos</abbr></a>
                                        </li>
                                        <li>
                                            <a href="dealers.php">
                                                <abbr title="Mirá donde podes conseguir nuestros productos">Dealers</abbr>
                                            </a>
                                        </li>
                                    </ul>
                                </aside>
                                <!-- /Widget: Meta -->

                                <!-- Widget: Search -->
                                <!--<aside class="widget widget_search">
                                    <form role="search" method="get" class="search_form" action="#">
                                        <input type="text" class="search_field" placeholder="Search &hellip;" value="" name="s" title="Search for:" />
                                        <button type="submit" class="search_button"></button>
                                    </form>
                                </aside>-->
                                <!-- /Widget: Search -->

                                <!-- Widget: Recent Posts -->
                                <!--<aside class="widget widget_recent_entries">
                                    <h5 class="widget_title">Recientes</h5>
                                    <ul>
                                        <li>
                                            <a href="post-single.html">Instructions &#038; Training</a>
                                        </li>
                                        <li>
                                            <a href="post-single.html">Rental Firearms &#038; Fees</a>
                                        </li>
                                        <li>
                                            <a href="post-single.html">Gun Range</a>
                                        </li>
                                    </ul>
                                </aside>-->
                                <!-- /Widget: Recent Posts -->

                                <!-- Widget: Tags -->
                                <aside class="widget widget_tag_cloud">
                                    <h5 class="widget_title">Tags</h5>
                                    <div class="tagcloud">
                                      <a href="#" title="5 topics">balines</a>
                                      <a href="#" title="5 topics">exposiciones</a>
                                      <a href="#" title="5 topics">exposiciones</a>
                                      <a href="#" title="1 topic">outdoor</a>
                                      <a href="#" title="1 topic">rifles</a>

                                      <!--
                                        English tags!
                                        <a href="#" title="5 topics">airguns</a>
                                        <a href="#" title="5 topics">archery</a>
                                        <a href="#" title="5 topics">camping</a>
                                        <a href="#" title="1 topic">class</a>
                                        <a href="#" title="5 topics">Classes</a>
                                        <a href="#" title="1 topic">outdoor</a>
                                        <a href="#" title="1 topic">gun</a>
                                        <a href="#" title="1 topic">holy</a>
                                        <a href="#" title="4 topics">hunting</a>
                                        <a href="#" title="1 topic">lecture</a>
                                        <a href="#" title="5 topics">optics</a>
                                        <a href="#" title="10 topics">Our Clients</a>
                                        <a href="#" title="6 topics">Our Shop</a>
                                        <a href="#" title="10 topics">post format</a>
                                        <a href="#" title="7 topics">protection</a>
                                        <a href="#" title="1 topic">rifles</a>
                                        <a href="#" title="5 topics">tactical</a>
                                        <a href="#" title="1 topic">theory</a>-->

                                    </div>
                                </aside>
                                <!-- /Widget: Tags -->
                            </div>
                        </div>
                        <!-- /Sidebar -->
                    </div>
                </div>
                <!-- /Page Content -->
                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>
        <script type="text/javascript" src="../include/js/vendor/swiper/swiper.js"></script>
        <script type="text/javascript" src="../include/js/postDetail.js"></script>

    </body>

</html>
