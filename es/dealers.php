<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Dealers</title>

        <?php include 'views/libs/header_includes.php'; ?>

    </head>

    <body class="page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/section_header.php'; ?>
                <!-- /Header Mobile -->
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">DEALERS</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.php">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span class="breadcrumbs_item current">DEALERS</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_no">
                    <!-- Content -->
                    <div class="content">
                        <article class="post_item post_item_single">
                            <section class="post_content">
                                <!-- Welcome to our store -->
                                <div class="empty_space height_4_8em"></div>
                                <div class="body_wrap">
                                    <!-- Page wrap -->
                                    <div class="">
                                      <div class="content_wrap">
                                        <h5 class="sc_title sc_title_regular">ENCONTRA NUESTROS PRODUCTOS EN LOS SIGUIENTES PUNTOS DE VENTA Y PAÍSES:</h5>

                                      <!-- Tabs -->
                                      <div class="empty_space height_2_65em"></div>
                                      <div id="sc_tabs_2" class="sc_tabs sc_tabs_style_1" data-active="0">
                                          <ul class="sc_tabs_titles">
                                              <li class="sc_tabs_title first">
                                                  <a href="#sc_tab_2_1" class="theme_button" id="sc_tab_2_1_tab">ARGENTINA</a>
                                              </li>
                                              <li class="sc_tabs_title">
                                                  <a href="#sc_tab_2_2" class="theme_button" id="sc_tab_2_2_tab">AMÉRICA</a>
                                              </li>
                                              <li class="sc_tabs_title last">
                                                  <a href="#sc_tab_2_3" class="theme_button" id="sc_tab_2_3_tab">AFRICA</a>
                                              </li>
                                              <li class="sc_tabs_title last">
                                                  <a href="#sc_tab_2_4" class="theme_button" id="sc_tab_2_3_tab">ASIA</a>
                                              </li>
                                              <li class="sc_tabs_title last">
                                                  <a href="#sc_tab_2_5" class="theme_button" id="sc_tab_2_3_tab">EUROPA</a>
                                              </li>
                                              <li class="sc_tabs_title last">
                                                  <a href="#sc_tab_2_6" class="theme_button" id="sc_tab_2_3_tab">OCEANÍA</a>
                                              </li>
                                          </ul>
                                          <div id="sc_tab_2_1" class="sc_tabs_content odd first">
                                            <h3 class="sc_title sc_title_regular margin_bottom_small">ARGENTINA</h3>
                                              <?php include 'views/argentina_tab_view.php'; ?>
                                          </div>
                                          <div id="sc_tab_2_2" class="sc_tabs_content even">
                                            <h3 class="sc_title sc_title_regular margin_bottom_small">AMÉRICA</h3>
                                              <?php include 'views/america_tab_view_new.php'; ?>
                                          </div>
                                          <div id="sc_tab_2_3" class="sc_tabs_content odd">
                                            <h3 class="sc_title sc_title_regular margin_bottom_small">AFRICA</h3>
                                              <?php include 'views/africa_tab_view_new.php'; ?>
                                          </div>
                                          <div id="sc_tab_2_4" class="sc_tabs_content even">
                                            <h3 class="sc_title sc_title_regular margin_bottom_small">ASIA</h3>
                                            <?php include 'views/asia_tab_view_new.php'; ?>
                                            </div>
                                          <div id="sc_tab_2_5" class="sc_tabs_content odd">
                                            <h3 class="sc_title sc_title_regular margin_bottom_small">EUROPA</h3>
                                            <?php include 'views/europe_tab_view_new.php'; ?>
                                          </div>
                                          <div id="sc_tab_2_6" class="sc_tabs_content even">
                                            <h3 class="sc_title sc_title_regular margin_bottom_small">OCEANÍA</h3>
                                            <?php include 'views/oceania_tab_view_new.php'; ?>
                                          </div>
                                      </div>
                                      <div class="empty_space height_2_65em"></div>
                                      <!-- /Tabs -->

                                    </div>
                                    </div>
                                </div>
                                <div class="empty_space height_3_9em"></div>
                            </section>
                        </article>
                    </div>
                </div>
                <!-- /Page Content -->

                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>
        <script type="text/javascript" src="../include/js/tabs-map.js"></script>

        <script async defer
	    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDu86P-x4XFnaKcsaZLBCOL-pHgZi6zFTo&callback=initMap">
        </script>

    </body>

</html>
