<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Contactanos</title>

        <?php include 'views/libs/header_includes.php'; ?>

        <style>
         /* Set the size of the div element that contains the map */
        #map {
        	height: 400px;  /* The height is 400 pixels */
        	width: 100%;  /* The width is the width of the web page */
         }
        </style>

    </head>

    <body class="body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/section_header.php'; ?>
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">CONTACTANOS</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.html">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span class="breadcrumbs_item current">CONTACTANOS</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_no">
                    <div class="content">
                        <article class="post_item post_item_single">
                            <section class="post_content">
                                <!-- Google Map -->
                                <div class="column-8_8" id="map"></div>
                                <!-- /Google Map -->
                                <!-- Contact Us Today -->
                                <div class="content_wrap">
                                    <div class="empty_space height_6_75em"></div>
                                    <div id="sc_form_2_wrap" class="sc_form_wrap">
                                        <div id="sc_form_2" class="sc_form sc_form_style_form_2">
                                            <h2 class="sc_form_title sc_item_title">
                                                CONTACTANOS
                                                <span class="thin"> HOY!</span>
                                            </h2>
                                            <div class="sc_form_descr sc_item_descr">
                                                <p class="margin_bottom_null">SU CORREO ELECTRONICO NO SERA PUBLICADO.</p>
                                                <p>LOS CAMPOS REQUERIDOS SON MARCADOS CON *</p>
                                            </div>
                                            <div class="sc_columns columns_wrap">
                                                <div class="sc_form_address column-1_3">
                                                    <div class="sc_form_address_field">
                                                        <span class="sc_form_address_label">Dirección</span>
                                                        <span class="sc_form_address_data">2553 - Justiniano Posse, Córdoba, Argentina., <br/>Av. Int. Pedro Paoloni 1546 - Parque Industrial</span>
                                                    </div>
                                                    <div class="sc_form_address_field">
                                                        <span class="sc_form_address_label">Teléfono</span>
                                                        <span class="sc_form_address_data">(+54)-3537-430320,</span><br/>
                                                        <img src="../content/image/whatsapp_icon.png" width="30" height="auto"/>
                                                        <span class="sc_form_address_data">(+54)-3537-683320</span>
                                                    </div>
                                                    <div class="sc_form_address_field">
                                                        <span class="sc_form_address_label">E-mail</span>
                                                        <span class="sc_form_address_data">info@balinesapolo.com.ar</span>
                                                    </div>
                                                    <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_small">
                                                      <div class="sc_socials_item">
                                                          <a href="https://www.facebook.com/BalinesApolo" target="_blank" class="social_icons social_facebook">
                                                              <span class="icon-facebook"></span>
                                                          </a>
                                                      </div>
                                                      <div class="sc_socials_item">
                                                          <a href="https://twitter.com/apolobalines" target="_blank" class="social_icons social_twitter">
                                                              <span class="icon-twitter"></span>
                                                          </a>
                                                      </div>
                                                      <div class="sc_socials_item">
                                                          <a href="https://www.instagram.com/balines.apolo/" target="_blank" class="social_icons social_instagram">
                                                              <span class="icon-instagramm"></span>
                                                          </a>
                                                      </div>
                                                    </div>
                                                </div><div class="sc_form_fields column-2_3">
                                                    <form id="sc_form_2_form" data-formtype="form_2" method="post" action="../include/sendmail.php">
                                                        <div class="sc_form_info">
                                                            <div class="sc_form_item sc_form_field label_over">
                                                                <label class="required" for="sc_form_username">Nombre</label>
                                                                <input id="sc_form_username" type="text" name="username" placeholder="Nombre">
                                                            </div>
                                                            <div class="sc_form_item sc_form_field label_over">
                                                                <label class="required" for="sc_form_email">E-mail</label>
                                                                <input id="sc_form_email" type="text" name="email" placeholder="E-mail">
                                                            </div>
                                                            <div class="sc_form_item sc_form_field label_over">
                                                                <label class="required" for="sc_form_subj">Asunto</label>
                                                                <input id="sc_form_subj" type="text" name="subject" placeholder="Asunto">
                                                            </div>
                                                        </div>
                                                        <div class="sc_form_item sc_form_message label_over">
                                                            <label class="required" for="sc_form_message">Mensaje</label>
                                                            <textarea id="sc_form_message" name="message" placeholder="Mensaje"></textarea>
                                                        </div>
                                                        <div class="sc_form_item sc_form_button">
                                                            <input type="submit" value="Enviar Mensaje" class="wpcf7-form-control wpcf7-submit" />
                                                        </div>
                                                        <div class="result sc_infobox"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="empty_space height_8_7em"></div>
                                </div>
                                <!-- /Contact Us Today -->
                            </section>
                        </article>
                    </div>
                </div>
                <!-- /Page Content -->
                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>
        <script>
            function initMap() {
            var map;
            var iconBase = '../content/image/marker.png';
            var infowindow = new google.maps.InfoWindow();

      	    map = new google.maps.Map(document.getElementById('map'), {
      		    zoom: 15,
      		    center: new google.maps.LatLng(-32.8783254,-62.6869204),
      		    mapTypeId: 'roadmap',
            streetViewControl: false,
            mapTypeControl: false,
            fullscreenControl: false,
            scaleControl: false,
            zoomControl: false
      	    });

            var latLng = new google.maps.LatLng(-32.8763138,-62.6878305);
            var content = "<div class=''><b>Balines Apolo</b></div><br/>Av. Int. Pedro Paoloni 1546 - Parque Industrial<br/>2553 - Justiniano Posse, Córdoba, Argentina.";
            var title = "Balines Apolo";

    	    var marker = new google.maps.Marker({
    		    position: latLng,
    		    map: map,
    		    icon: iconBase,
    		    title: title
            });
        
    	    google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
    		    return function() {
    			    infowindow.setContent(content);
    			    infowindow.open(map,marker);
    		    };
    	    })(marker,content,infowindow));
      	}
        </script>

        <script async defer
        	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDu86P-x4XFnaKcsaZLBCOL-pHgZi6zFTo&callback=initMap">
        </script>
    </body>
</html>
