<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Empresa</title>

        <?php include 'views/libs/header_includes.php'; ?>

    </head>

    <body class="page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/section_header.php'; ?>
                <!-- /Header Mobile -->
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">EMPRESA</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.php">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span class="breadcrumbs_item current">EMPRESA</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_no">
                    <!-- Content -->
                    <div class="content">





                        <article class="post_item post_item_single">
                            <section class="post_content">
                                <!-- Welcome to our store -->
                                <div class="empty_space height_4_8em"></div>
                                <div class="sc_section">
                                    <div class="content_wrap">
                                      <!-- Video Player -->
                                      <div class="empty_space height_2_65em"></div>
                                      <div class="sc_video_player">
                                        <div class="sc_video_frame sc_video_play_button hover_icon hover_icon_play" data-width="100%" data-height="659" data-video="&lt;iframe class=&quot;video_frame&quot;
                                        src=&quot;https://player.vimeo.com/video/381320004?autoplay=1&quot; width=&quot;100%&quot; height=&quot;659&quot; frameborder=&quot;0&quot; webkitAllowFullScreen=&quot;webkitAllowFullScreen&quot; mozallowfullscreen=&quot;mozallowfullscreen&quot; allowFullScreen=&quot;allowFullScreen&quot;&gt;&lt;/iframe&gt;">
                                            <img alt="" src="../content/image/BalinesApoloCaptura-1024x788.png">
                                        </div>
                                      </div>
                                      <div class="empty_space height_2_65em"></div>
                                      <!-- /Video Player -->
                                        <div class="sc_section_inner">
                                            <h2 class="sc_section_title sc_item_title">BIENVENIDO
                                                <span class="thin"> A BALINES APOLO</span>
                                            </h2>
                                            <div class="sc_section_descr sc_item_descr">
                                                <p class="margin_bottom_null">Fabrica De balines Apolo es una empresa que desde el año 2001 se encuentra en constante Evolución e Innovación… siendo una de las pocas en el rubro que ha logrado Certificaciones de calidad y medio Ambiente a nivel Internacional.( 14001 y 9001).</p>
                                            </div>
                                            <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_2 responsive_columns margin_top_medium margin_bottom_medium">
                                                <div class="column-1_2 sc_column_item sc_column_item_1 odd first">
                                                    <span class="sc_icon icon-iconmonstr-crosshair-7-icon alignleft margin_top_null margin_bottom_null tpl_icons_style_1"></span>
                                                    <p>En la actualidad, se fabrican 3 Líneas de Balines de Plomo para Armas de  Aire Comprimidos y 1 línea de fabricación de balines de Acero cobreados (BBs).</p>
                                                    <div class="empty_space height_2_4em"></div>
                                                    <span class="sc_icon icon-binocle alignleft margin_top_null margin_bottom_null tpl_icons_style_1"></span>
                                                    <p>Además, contamos con sistema de baño de Cobre especial, dado que el mismo no se oxida y con el tiempo sigue teniendo el mismo brillo y excelente presentación en los balines, con este recubrimiento hemos logrado en algunos modelos de balines hasta un 20% mas de Velocidad.</p>
                                                    <div class="empty_space height_2_4em"></div>
                                                    <!--<span class="sc_icon icon-iconmonstr-police-weapon-icon alignleft margin_top_null margin_bottom_null tpl_icons_style_1"></span>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliminim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
                                                </div><div class="column-1_2 sc_column_item sc_column_item_2 even">
                                                    <figure class="sc_image sc_image_shape_square">
                                                        <img src="../content/image/image_enterprise.jpg" alt="" />
                                                    </figure>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="empty_space height_3_9em"></div>
                                <!-- /Welcome to our store -->
                                <!-- Our team -->
                                <div class="bg_dark_style_1">
                                    <div class="content_wrap">
                                        <div class="empty_space height_6_9em"></div>
                                        <div class="sc_team_wrap">
                                            <div  class="sc_team sc_team_style_team-1">
                                                <h2 class="sc_team_title sc_item_title">NUESTRAS
                                                    <span class="thin"> LÍNEAS DE PRDOUCCIÓN</span>
                                                </h2>
                                                <div class="sc_team_descr sc_item_descr">
                                                    <p class="margin_bottom_null"></p>
                                                </div>
                                                <div class="sc_columns columns_wrap">
                                                    <div class="column-1_3 column_padding_bottom">
                                                        <div class="sc_team_item sc_team_item_1 odd first">
                                                            <!--<div class="sc_team_item_avatar">
                                                                <img alt="" src="http://placehold.it/370x370.jpg"></div>-->
                                                            <div class="sc_team_item_info">
                                                                <h4 class="sc_team_item_title">
                                                                    <a href="shop.php?category=Estandar">Línea Estandar o Conic</a>
                                                                </h4>
                                                                <div class="sc_team_item_description">Estos balines tienen una excelente relación calidad-precio para el divertimento o plinking.</div>
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><div class="column-1_3 column_padding_bottom">
                                                        <div class="sc_team_item sc_team_item_2 even">
                                                            <!--<div class="sc_team_item_avatar">
                                                                <img alt="" src="http://placehold.it/370x370.jpg">
                                                            </div>-->
                                                            <div class="sc_team_item_info">
                                                                <h4 class="sc_team_item_title">
                                                                    <a href="shop.php?category=Premium">Línea Premium</a>
                                                                </h4>
                                                                <div class="sc_team_item_description">
                                                                    La segunda Línea denominada Premium, son balines estampados de Alta Gama, de diferentes calibres, pesos y puntas. Pudiendo abarcar casi todo el abanico de posibilidad que un amante a la caza o al tiro al Blanco puede necesitar.</div>
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><div class="column-1_3 column_padding_bottom">
                                                        <div class="sc_team_item sc_team_item_3 odd">
                                                            <!--<div class="sc_team_item_avatar">
                                                                <img alt="" src="http://placehold.it/370x370.jpg">
                                                            </div>-->
                                                            <div class="sc_team_item_info">
                                                                <h4 class="sc_team_item_title">
                                                                    <a href="shop.php?category=AirBoss">Línea AIR BOSS</a>
                                                                </h4>
                                                                <div class="sc_team_item_description">Y nuestra ultima Línea, denomina AIR BOSS, si bien también son estampados, los mismos son seleccionados por una Línea de inspección automática con Laser infrarojo, teniendo dicha medición una tolerancia de 0.004mm. por lo que hace que sea unos de los balines más Precisos del mercado, teniendo con ellos excelentes agrupaciones tanto en Competencia como en cacería con los nuevos Rifles de PCP.</div>
                                                                <div class="sc_socials sc_socials_type_icons sc_socials_shape_square sc_socials_size_tiny">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="empty_space height_5em"></div>
                                    </div>
                                </div>
                                <!-- /Our team -->



                                <!-- Book Right Now  book_now_section_bg -->
                                <div class="responsive_bg">
                                    <div class="content_wrap">
                                      <!-- Countdown -->
                                      <div class="empty_space height_2_65em"></div>
                                      <h3 class="sc_title sc_title_regular margin_bottom_small">NUESTROS NÚMEROS</h3>
                                      <div class="columns_wrap sc_columns columns_nofluid sc_columns_count_4 counters_section">
                                          <div class="column-1_4 sc_column_item sc_column_item_1 odd first">
                                              <div class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                                  <div class="sc_skills_item sc_skills_style_1 odd first">
                                                      <div class="sc_skills_icon icon-iconmonstr-crosshair-7-icon"></div>
                                                      <div class="sc_skills_count">
                                                          <div class="sc_skills_total" data-start="0" data-stop="50000000" data-step="200000" data-max="50000000" data-speed="16" data-duration="1349" data-ed="">0</div>
                                                      </div>
                                                      <div class="sc_skills_info">
                                                          <div class="sc_skills_label">BALINES POR MES</div>
                                                      </div>
                                                      <a class="sc_skills_read_more" href="#">
                                                          <span class="icon-right"></span>
                                                      </a>
                                                  </div>
                                              </div>
                                          </div><div class="column-1_4 sc_column_item sc_column_item_2 even">
                                          <div class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                              <div class="sc_skills_item sc_skills_style_1 odd first">
                                                  <div class="sc_skills_icon icon-database-light"></div>
                                                  <div class="sc_skills_count">
                                                      <div class="sc_skills_total" data-start="0" data-stop="6000" data-step="60" data-max="6000" data-speed="22" data-duration="2200" data-ed="">0</div>
                                                  </div>
                                                  <div class="sc_skills_info">
                                                      <div class="sc_skills_label">LATAS POR DÍA</div>
                                                  </div>
                                                  <a class="sc_skills_read_more" href="#">
                                                      <span class="icon-right"></span>
                                                  </a>
                                              </div>
                                          </div>
                                      </div><div class="column-1_4 sc_column_item sc_column_item_3 odd">
                                          <div class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                              <div class="sc_skills_item sc_skills_style_1 odd first">
                                                  <div class="sc_skills_icon icon-location"></div>
                                                  <div class="sc_skills_count">
                                                      <div class="sc_skills_total" data-start="0" data-stop="42" data-step="2" data-max="42" data-speed="100" data-duration="20" data-ed="">0</div>
                                                  </div>
                                                  <div class="sc_skills_info">
                                                      <div class="sc_skills_label">PAISES</div>
                                                  </div>
                                                  <a class="sc_skills_read_more" href="#">
                                                      <span class="icon-right"></span>
                                                  </a>
                                              </div>
                                          </div>
                                      </div><div class="column-1_4 sc_column_item sc_column_item_4 even">
                                          <div class="sc_skills sc_skills_counter" data-type="counter" data-caption="Skills">
                                              <div class="sc_skills_item sc_skills_style_1 odd first">
                                                  <div class="sc_skills_icon icon-calendar-light"></div>
                                                  <div class="sc_skills_count">
                                                      <div class="sc_skills_total" data-start="0" data-stop="18" data-step="1" data-max="18" data-speed="100" data-duration="20" data-ed="">0</div>
                                                  </div>
                                                  <div class="sc_skills_info">
                                                      <div class="sc_skills_label">AÑOS DE TRAYECTORIA</div>
                                                  </div>
                                                  <a class="sc_skills_read_more" href="#">
                                                      <span class="icon-right"></span>
                                                  </a>
                                              </div>
                                          </div>
                                      </div>
                                      </div>
                                      <div class="empty_space height_2_65em"></div>
                                      <!-- /Countdown -->
                                    </div>
                                </div>
                                <!-- /Book Right Now -->
                            </section>
                        </article>
                    </div>
                </div>
                <!-- /Page Content -->

                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>

    </body>

</html>
