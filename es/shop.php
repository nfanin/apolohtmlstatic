<!DOCTYPE html>
<html lang="en-US" class="scheme_original">

    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Productos</title>

        <?php include 'views/libs/header_includes.php'; ?>

    </head>
    <body class="woocommerce woocommerce-page body_filled article_style_stretch scheme_original top_panel_show top_panel_above sidebar_show sidebar_left">
        <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap bg_image">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/section_header.php'; ?>
                <!-- /Header -->
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1 title_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <h1 class="page_title">BALINES</h1>
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.php">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span class="breadcrumbs_item current">BALINES</span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_yes">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content">
                            <div class="list_products shop_mode_thumbs">
                                <!--<div class="mode_buttons">
                                    <a href="#" class="woocommerce_thumbs icon-th" title="Show products as thumbs"></a>
                                    <a href="shop-list.html" class="woocommerce_list icon-th-list" title="Show products as list"></a>
                                </div>-->
                                <p id="showing_results" class="woocommerce-result-count">
                                     Mostrando 39 de 47 resultados</p>
                                <!--<form class="woocommerce-ordering" method="get">
                                    <select name="orderby" class="orderby">
                                        <option value="menu_order" selected='selected'>Default sorting</option>
                                        <option value="popularity">Sort by popularity</option>
                                        <option value="rating">Sort by average rating</option>
                                        <option value="date">Sort by newness</option>
                                        <option value="price">Sort by price: low to high</option>
                                        <option value="price-desc">Sort by price: high to low</option>
                                    </select>
                                </form>-->
                                <!-- Products List -->
                                <ul id="product_list_id" class="products">

                                </ul>
                                <!-- /Products List -->
                                <!-- Pagination -->
                                <!--<nav class="pagination_wrap pagination_pages">
                                    <span class="pager_current active">1</span>
                                    <a href="#" class="">2</a>
                                    <a href="#" class="pager_next ">&#8250;</a>
                                    <a href="#" class="pager_last ">&raquo;</a>
                                </nav>-->
                                <!-- /Pagination -->
                            </div>
                        </div>
                        <!-- /Content -->
                        <!-- Sidebar -->
                        <div class="sidebar widget_area scheme_original">
                            <div class="sidebar_inner widget_area_inner">
                                <!-- Widget: Product Categories -->
                                <aside class="widget woocommerce widget_product_categories">
                                    <h5 class="widget_title">CATEGORÍAS</h5>
                                    <ul class="product-categories">
                                        <li>
                                        <a href="javascript:void(0);" id="Slug" onclick="filterProducts('Slug');">SLUG</a>
                                            <ul class="children">
                                                <li>
                                                    <a href="javascript:void(0);" id="Slug22" onclick="filterProducts('Slug22');">Cal.22 - 5,5</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="Slug25" onclick="filterProducts('Slug25');">Cal.25 - 6,35</a>
                                                </li>
                                              </ul>
                                        </li>
                                        <li><a href="javascript:void(0);" id="SteelCopper" onclick="filterProducts('SteelCopper');">BBS STEEL COPPER</a>
                                            <ul class="children">
                                                <li>
                                                    <a href="javascript:void(0);" id="SteelCopper177" onclick="filterProducts('SteelCopper177');">Cal.177 - 4,5</a>
                                                </li>
                                              </ul>
                                        </li>
                                        <li><a href="javascript:void(0);" id="AirBoss" onclick="filterProducts('AirBoss');">BALINES AIR BOSS</a>
                                            <ul class="children">
                                                <li>
                                                <a href="javascript:void(0);" id="AirBoss177" onclick="filterProducts('AirBoss177');">Cal.177 - 4,5</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="AirBoss22" onclick="filterProducts('AirBoss22');">Cal.22 - 5,5</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="AirBoss25" onclick="filterProducts('AirBoss25');">Cal.25 - 6,35</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="AirBoss30" onclick="filterProducts('AirBoss30');">Cal. 30 - 7,62</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="AirBoss35" onclick="filterProducts('AirBoss35');">Cal. 35 - 9,00</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:void(0);" id="Premium" onclick="filterProducts('Premium');">BALINES PREMIUM</a>
                                            <ul class="children">
                                                <li>
                                                <a href="javascript:void(0);" id="Premium177" onclick="filterProducts('Premium177');">Cal.177 - 4,5</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="Premium22" onclick="filterProducts('Premium22');">Cal.22 - 5,5</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="Premium25" onclick="filterProducts('Premium25');">Cal.25 - 6,35</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="javascript:void(0);" id="Estandar" onclick="filterProducts('Estandar');">BALINES ESTANDAR</a>
                                            <ul class="children">
                                                <li>
                                                <a href="javascript:void(0);" id="Estandar177" onclick="filterProducts('Estandar177');">Cal.177 - 4,5</a>
                                                </li>
                                                <li>
                                                <a href="javascript:void(0);" id="Estandar22" onclick="filterProducts('Estandar22');">Cal.22 - 5,5</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </aside>
                                <!-- /Widget: Product Categories -->
                                <!-- Widget: Product Tag -->
                                <aside class="widget woocommerce widget_product_tag_cloud">
                                    <h5 class="widget_title">BALINES POR CALIBRE</h5>
                                    <div class="tagcloud">
                                        <a href="javascript:void(0);" id="177" onclick="filterProducts('177');" title="">Cal.177 - 4,5</a>
                                        <a href="javascript:void(0);" id="22" onclick="filterProducts('22');" title="">Cal.22 - 5,5</a>
                                        <a href="javascript:void(0);" id="25" onclick="filterProducts('25');" title="">Cal.25 - 6,35</a>
                                        <a href="javascript:void(0);" id="30" onclick="filterProducts('30');" title="">Cal.30 - 7,62</a>
                                        <a href="javascript:void(0);" id="35" onclick="filterProducts('35');" title="">Cal.35 - 9,00</a>
                                    </div>
                                </aside>
                                <!-- /Widget: Product Tag -->
                            </div>
                        </div>
                        <!-- /Sidebar -->
                    </div>
                </div>
                <!-- /Page Content -->
                <!-- Footer -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer -->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>
        <script type="text/javascript" src="../include/js/product.js"></script>

    </body>

</html>
