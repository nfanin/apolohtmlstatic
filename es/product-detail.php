<!DOCTYPE html>
<html lang="en-US" class="scheme_original">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no">

        <title>Balines Apolo &#8211; Productos</title>

        <?php include 'views/libs/header_includes.php'; ?>

    </head>
    <body class="single-product woocommerce woocommerce-page body_transparent article_style_stretch scheme_original top_panel_show top_panel_above sidebar_hide">
    <div id="page_preloader"></div>
        <!-- Body wrap -->
        <div class="body_wrap bg_image">
            <!-- Page wrap -->
            <div class="page_wrap">
                <!-- Header -->
                <?php include 'views/section_header.php'; ?>
                <!-- /Header Mobile -->
                <!-- Breadcrumbs -->
                <div class="top_panel_title top_panel_style_1  title_present navi_present breadcrumbs_present scheme_original">
                    <div class="top_panel_title_inner top_panel_inner_style_1">
                        <div class="content_wrap">
                            <div class="breadcrumbs">
                                <a class="breadcrumbs_item home" href="index.php">HOME</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <a class="breadcrumbs_item all" href="shop.php">BALINES</a>
                                <span class="breadcrumbs_delimiter"></span>
                                <a id="pellet_category" class="breadcrumbs_item all" href="shop.php"></a>
                                <span class="breadcrumbs_delimiter"></span>
                                <span id="pellet_name_category" class="breadcrumbs_item current">< Category ></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Breadcrumbs -->
                <!-- Page Content -->
                <div class="page_content_wrap page_paddings_yes">
                    <div class="content_wrap">
                        <!-- Content -->
                        <div class="content">
                            <article class="post_item post_item_single post_item_product">
                                <div class="product">
                                    <!-- Product Image -->
                                    <div class="woocommerce-product-gallery woocommerce-product-gallery--with-images woocommerce-product-gallery--columns-4 images" data-columns="4" style="opacity: 1; transition: opacity .25s ease-in-out;">
                                        <figure class="woocommerce-product-gallery__wrapper">

                                              <img id="image_product_detail" src="" alt="" title=""/>

                                        </figure>
                                    </div>
                                    <!-- /Product Image -->
                                    <!-- Product Summary -->
                                    <div class="summary entry-summary">
                                        <h1 id="name_product_detail" class="product_title"></h1>
                                        <p class="price">
                                            <span class="woocommerce-Price-amount amount">
                                                <span class="woocommerce-Price-currencySymbol"></span>ESPECIFICACIONES
                                            </span>
                                        </p>
                                        <div class="woocommerce-product-details__short-description">
                                            <p><b>Calibre:</b> <span id="caliber_product_detail"></span><br/>
                                            <span><b>Peso:</b> <span id="weight_product_detail"></span></span></p>
                                            <p id="especification_product_detail"> </p>
                                            <p id="presentation_product_detail"> </p>
                                        </div>
                                        <div class="product_meta">
                                            <span class="posted_in">Categorias:
                                                <a id="categories_product_detail" href="#"></a>
                                            </span>
                                            <span class="product_id">ID de Producto:
                                                <span id="product_detail_id"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- /Product Summary -->
                                    <!-- /Tabs -->
                                </div>
                            </article>
                        </div>
                        <!-- /Content -->
                    </div>
                </div>
                <!-- /Page Content -->
                <!-- Footer 1 -->
                <?php include 'views/footer.php'; ?>
                <!-- /Footer 2-->
            </div>
            <!-- /Page wrap -->
        </div>
        <!-- /Body wrap -->

        <a href="#" class="scroll_to_top icon-up" title="Scroll to top"></a>

        <?php include 'views/libs/footer_includes.php'; ?>
        <script type="text/javascript" src="../include/js/productDetail.js"></script>


    </body>

</html>
